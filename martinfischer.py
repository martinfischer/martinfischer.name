# martinfischer.name/index.html
# Die Webseite von Martin Fischer (Python 3 Version)

martin = {
    "Name": "Martin Fischer",
    "E-Mail": "martin@fischerweb.net",
    "Github": "github.com/martinfischer",
    "Fähigkeiten": [
        "Python",
        "Django",
        "PHP",
        "Wordpress",
        "SQL",
        "CSS",
        "Bootstrap",
        "Linux",
        "git",
        "Javascript",
        "jQuery",
        "Java",
        "Sehr gutes Englisch in Wort und Schrift"
    ],
    "Alter": 37,
    "Wohnort": "St. Johann in Tirol"
}

gesuchte_fähigkeit = "" # Welche Fähigkeit brauchen Sie?

if gesuchte_fähigkeit in martin["Fähigkeiten"]:
    with open("Kontaktdaten-Martin-Fischer.txt", "w") as kontakt:
        kontakt.write("Name: {name}\n".format(name=martin["Name"]))
        kontakt.write("E-Mail: {email}\n".format(email=martin["E-Mail"]))
        kontakt.write("Github: {github}\n".format(github=martin["Github"]))
        kontakt.write("Alter: {alter}\n".format(alter=martin["Alter"]))
        kontakt.write("Wohnort: {wohnort}".format(wohnort=martin["Wohnort"]))

# Impressum


################################################################################


# martinfischer.name/index.html
# A website about Martin Fischer (Python 3 version)

martin = {
    "Name": "Martin Fischer",
    "E-Mail": "martin@fischerweb.net",
    "Github": "github.com/martinfischer",
    "Skills": [
        "Python",
        "Django",
        "PHP",
        "Wordpress",
        "SQL",
        "CSS",
        "Bootstrap",
        "Linux",
        "git",
        "Javascript",
        "jQuery",
        "Java",
        "Very good English and German native speaker"
    ],
    "Age": 37,
    "City": "St. Johann in Tirol, Austria"
}

wanted_skill = "" # Which skill do you need?

if wanted_skill in martin["Skills"]:
    with open("Contact-Details-Martin-Fischer.txt", "w") as contact:
        contact.write("Name: {name}\n".format(name=martin["Name"]))
        contact.write("E-Mail: {email}\n".format(email=martin["E-Mail"]))
        contact.write("Github: {github}\n".format(github=martin["Github"]))
        contact.write("Age: {age}\n".format(age=martin["Age"]))
        contact.write("City: {city}".format(city=martin["City"]))

# Imprint
