$(function() {

    $(".english").click(function() {
        $("#tab-de").css("display", "none");
        $("#tab-en").css("display", "inline");
        $(".english").addClass("current-tab");
        $(".deutsch").removeClass("current-tab");
        $("#input-en").val("").css("border", "2px solid #36649A");
    });

    $(".deutsch").click(function() {
        $("#tab-en").css("display", "none");
        $("#tab-de").css("display", "inline");
        $(".deutsch").addClass("current-tab");
        $(".english").removeClass("current-tab");
        $("#input-de").val("").css("border", "2px solid #36649A");
    });

    var skills = [
        "python",
        "django",
        "php",
        "wordpress",
        "sql",
        "css",
        "bootstrap",
        "linux",
        "git",
        "javascript",
        "jquery",
        "java",
        "sehr gutes englisch in wort und schrift",
        "very good english and german native speaker"
    ];

    $("#run-de").click(function() {
        var input = $("#input-de").val();
        var bool = skills.indexOf(input.toLowerCase()) >= 0;
        if (bool) {
            $("#input-de").css("border", "2px solid green");
            var link = document.createElement("a");
            link.href = "files/Kontaktdaten-Martin-Fischer.txt";
            link.download = "Kontaktdaten-Martin-Fischer.txt";
            document.body.appendChild(link);
            link.click();
        }
        else {
            $("#input-de").css("border", "2px solid red");
        };
    });

    $("#run-en").click(function() {
        var input = $("#input-en").val();
        var bool = skills.indexOf(input.toLowerCase()) >= 0;
        if (bool) {
            $("#input-en").css("border", "2px solid green");
            var link = document.createElement("a");
            link.href = "files/Contact-Details-Martin-Fischer.txt";
            link.download = "Contact-Details-Martin-Fischer.txt";
            document.body.appendChild(link);
            link.click();
        }
        else {
            $("#input-en").css("border", "2px solid red");
        };
    });

    $("#input-de").focus(function() {
        $("#input-de").css("border", "2px solid #36649A");
    });

});